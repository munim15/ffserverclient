package com.munim.ffchallenge;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.TextView;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Main Activity for FF Server Request app
 * @author Munim Ali
 */
public class MainActivity extends Activity {

    /** Most recent response from server */
    String curr_resp;
    /** Most recent message from user*/
    private String curr_msg;
    /** AlertDialog builder for error notification */
    private AlertDialog.Builder builder;
    /** Hard Coded messages for error codes 1-5 as the server doesn't seem to respond with messages*/
    private String[] serverErrors;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        serverErrors = new String[] {"Request received has a missing 'func' parameter.",
                                     "The 'func' value you specified is a function that does not exist.",
                                     "'data' parameter is required to call the function but was not provided.",
                                     "Malformed JSON data for the 'data' parameter.",
                                     "'uuid' parameter is required but was not provided."};
        final EditText req_et = (EditText) findViewById(R.id.message);
        final TextView resp_et = (TextView) findViewById(R.id.response);
        Button bttn = (Button) findViewById(R.id.send);
        bttn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                curr_msg = req_et.getText().toString();
                curr_resp = "";
                resp_et.setText(curr_resp);
                RequestTask r = new RequestTask();
                r.execute();
            }
        });
        builder = new AlertDialog.Builder(MainActivity.this);

    }

    /** Makes a request to API */
    private boolean makeRequest() {
        if (!isNetworkAvailable()) {
            Log.i("Main.makeRequest", "No network connectivity");
            curr_resp = String.format("%s#%s", "Error", "No Network Connectivity Detected");
            return false;
        }
        try {
            checkBoxes();
            String response = RequestHandler.testRequest("funcTest", 1337L,
                    new JSONObject().put("message", curr_msg));
            if (response == null) {
                curr_resp = String.format("%s#%s", "Error", "Connection Timed Out");
                return false;
            }
            JSONObject obj = new JSONObject(response);
            if (obj.getBoolean("success")) {
                curr_resp = "Response from Server: " + obj.getString("data");
                return true;
            } else {
                String message = obj.has("message") ? obj.getString("message") : serverErrors[obj.getInt("errCode") - 1];
                String title = obj.has("title") ? obj.getString("title") : "Error";
                curr_resp = String.format("%s#%s", title, message);
            }
            return false;
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return false;
    }

    /**
     * Checks for Network Availability
     */
    private boolean isNetworkAvailable() {
        ConnectivityManager connectivityManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }

    /**
     * Popup Dialog with title as TITLE
     * and message as MESSAGE
     */
    private void showAlert(String title, String message) {
        builder.setMessage(message)
                .setTitle(title)
                .setNeutralButton("Ok", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                    }
                });
        AlertDialog alert = builder.create();
        alert.show();
    }

    /**
     * Updates UI based on
     * success of request
     */
    private void update(boolean success) {
        if (success)
            ((TextView) findViewById(R.id.response)).setText(curr_resp);
        else {
            String[] args = curr_resp.split("#");
            showAlert(args[0], args[1]);
            curr_resp = "";
        }
    }

    private class RequestTask extends AsyncTask<Void, Void, Boolean> {

        private ProgressDialog dialog;

        RequestTask() {
            dialog = new ProgressDialog(MainActivity.this);
        }

        @Override
        protected void onPreExecute() {
            dialog.setMessage("Making request, Please wait ...");
            dialog.show();
        }

        @Override
        protected Boolean doInBackground(Void... params) {
            boolean response = makeRequest();
            try {
                Thread.sleep(500); // Adding small delta t
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            return response;
        }

        @Override
        protected void onPostExecute(Boolean success) {
            if (dialog.isShowing()) {
                dialog.dismiss();
            }
            update(success);
        }
    }

    /**
     * Setting errorType for RequestHandler
     * based on checked preferences.
     * NOTE : only for illustrative purpose,
     * errType should always be 0 otherwise
     */
    public void  checkBoxes() {
        if (!((CheckBox) findViewById(R.id.checkFunc)).isChecked()) {
            RequestHandler.setErrType(1);
        } else if (!((CheckBox) findViewById(R.id.checkUuid)).isChecked()) {
            RequestHandler.setErrType(5);
        } else if (!((CheckBox) findViewById(R.id.checkValid)).isChecked()) {
            RequestHandler.setErrType(2);
        } else if (!((CheckBox) findViewById(R.id.checkData)).isChecked()) {
            RequestHandler.setErrType(3);
        } else if (!((CheckBox) findViewById(R.id.checkJson)).isChecked()) {
            RequestHandler.setErrType(4);
        } else {
            RequestHandler.setErrType(0);
        }
    }

}
