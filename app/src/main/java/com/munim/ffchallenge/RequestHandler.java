package com.munim.ffchallenge;

import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.SocketTimeoutException;
import java.net.URL;
import javax.net.ssl.HttpsURLConnection;


/**
 * Handles requests to FF API server
 * @author Munim Ali
 */
public class RequestHandler {

    /** FF API Server URL*/
    private static final String base_url = "https://api.feedingforward.com/v1/";
    /** Format for API request payload*/
    private static String payload = "func=%s&uuid=%d&data=%s";
    /** Type of error. NOTE : only for illustrative purpose, should always be 0 otherwise */
    private static int errType;

    /**
     * Sends a test request to server.
     * NOTE: The connection has a 6000 ms timeout
     */
    public static String testRequest(String func, Long uuid, JSONObject data) {
        try {
            URL u = new URL(base_url);
            HttpsURLConnection conn = (HttpsURLConnection) u.openConnection();
            conn.setConnectTimeout(6000);
            conn.setRequestMethod("POST");
            conn.setDoOutput(true);
            DataOutputStream wr = new DataOutputStream(conn.getOutputStream());
            String params;
            // Only for illustrative purpose, should use default format otherwise:
            switch (errType) {
                case 1:
                    params = String.format("uuid=%d&data=%s", uuid, data.toString());
                    break;
                case 2:
                    params = String.format(payload, "blah", uuid, data.toString());
                    break;
                case 3:
                    params = String.format("func=%s&uuid=%d", func, uuid);
                    break;
                case 4:
                    params = String.format(payload, func, uuid, "}}{");
                    break;
                case 5:
                    params = String.format("func=%s&data=%s", func, data.toString());
                    break;
                default:
                    params = String.format(payload, func, uuid, data.toString());
                    break;
            }
            wr.writeBytes(params);
            wr.flush();
            wr.close();
            StringBuilder resp = new StringBuilder();
            BufferedReader br = new BufferedReader(new InputStreamReader(conn.getInputStream()));
            String line;
            while ( (line = br.readLine()) != null)
                resp.append(line);
            br.close();
            return resp.toString();
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (SocketTimeoutException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * Set Type of error. NOTE : only for illustrative purpose,
     * errType should always be 0 otherwise
     */
    public static void setErrType(int e) {
        errType = e;
    }

    // More methods go here ...
}

